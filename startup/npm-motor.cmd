require npm-motor, nblm_dev
require singlemotion,1.4.6+


# Set environmental variables
epicsEnvSet("ASYN_PORT", "GEOBRICK_ASYN")
epicsEnvSet("PMAC_IP", "10.10.3.42")
epicsEnvSet("PMAC_PORT", "1025")


# fonctions from TPMAC
# Connection to GEOBRICK, create a asyn port
pmacAsynIPConfigure($(ASYN_PORT), $(PMAC_IP):$(PMAC_PORT))

# load PMAC (geobrick) database
dbLoadRecords("npm-motor_get_value_pmac.db")
dbLoadRecords("npm-motor_set_value_pmac.db")
dbLoadRecords("npm-motor_console.db")

#communication PLC PMAC
# dbLoadRecords("npm-motor_set_bo_PLC.db")
# dbLoadRecords("npm-motor_set_bo_pmac.db")

dbLoadRecords("npm-motor_conversion.template")

