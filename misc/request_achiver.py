
import urllib
import json
from operator import itemgetter # sorted list of list
from collections import Counter # count occurences
import sys #exit script
import matplotlib.pyplot as plt
import time
import numpy as np

# color bar
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm
from matplotlib.ticker import MultipleLocator

#configuration
PREFIX = 'SOURCE_AUTO_PARAM:'
ADRESS_ARCHIVER = "132.166.31.140"
PORT_ARCHIVER = "17668"

# data type
NOT_A_LIST = 0
LIST = 1
# TYPE PV
SCALAR = 0
WAVEFORM = 1
IMAGE_2D = 2
SIZE_MIN_IMAGE2D = 960*600


def secondsToStrLocalTime (sec):
    """ seconds since the epoch to UTC time"""
    timeStucture = time.localtime(sec)
    return time.strftime("%Y-%m-%d %H:%M:%S", timeStucture) #ex: 2018-01-30 16:38:10

def getDataFromArchiver(pvName, pvType, dateTimeStart, dateTimeStop):
    startProgram = time.time()

    print "\nPV:", pvName

    print "start request to archiver.. (%f s)" %(time.time()-startProgram)
        # elapsed time between start end stop date
    # Parse a string representing a time according to a format.
    dateStartStructure = time.strptime(dateTimeStart, "%Y-%m-%d %H:%M:%S") 
    dateStopStructure = time.strptime(dateTimeStop, "%Y-%m-%d %H:%M:%S") 
    #check timeline: start date before stop date
    if(dateStartStructure > dateStopStructure):
        exit("FAILED: date start after date stop")
    elapsedTime = time.mktime(dateStopStructure) - time.mktime(dateStartStructure)

    # split date and time
    dateStart, timeStart = dateTimeStart.split(' ') #split date in yy/mm/day and hh:mm:ss
    dateStop, timeStop =dateTimeStop.split(' ')

    #UTC+2 could be different to localtime()
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = - (time.altzone if is_dst else time.timezone)
    utc_offset = utc_offset/3600 #secs to hours
    if utc_offset < 0:
        TIME_DIFFERENCE_WITH_UTC = '000-0' + str(utc_offset) + ':00'
    elif utc_offset > 0:
        TIME_DIFFERENCE_WITH_UTC = '000+0' + str(utc_offset) + ':00'
    else:
        TIME_DIFFERENCE_WITH_UTC = '000+00:00'
    #print TIME_DIFFERENCE_WITH_UTC

    #url construction, data in ISO 8601: yyyy-MM-dd"T"HH:mm:ss.SSSZZ
    dateTimeStart = dateStart + "T" + timeStart + "." + TIME_DIFFERENCE_WITH_UTC
    dateTimeStop  = dateStop  + "T" + timeStop + "." + TIME_DIFFERENCE_WITH_UTC
    #print dateTimeStart, dateTimeStop    


    # URL request
    foramt = "json" # "raw" "json"
    url = "http://" + ADRESS_ARCHIVER + ":" + PORT_ARCHIVER + "/retrieval/data/getData." + foramt + "?pv=" + urllib.quote_plus(pvName) + "&from=" + urllib.quote_plus(dateTimeStart) + "&to=" +  urllib.quote_plus(dateTimeStop)
    print "..end request to archiver (%f s)\n" %(time.time()-startProgram)

    # URL answer
    print "start receiving data from archiver.. (%f s)" %(time.time()-startProgram)
    req = urllib.urlopen(url)
    print "..end receiving data from archiver (%f s)\n" %(time.time()-startProgram)

    print "start load data.. (%f s)" %(time.time()-startProgram)
    try :
        data = json.load(req)
    except:
        print("ERROR: no data for the period asked or PV not archived")
        return [], []
    print "..end load data (%f s)\n" %(time.time()-startProgram)

    print "start split data received.. (%f s)" %(time.time()-startProgram)
    secs = [x["secs"] for x in data[0]["data"]]
    vals = [x["val"] for x in data[0]["data"]]
    nanos = [x["nanos"] for x in data[0]["data"]]

    if len(secs) == 0:
        exit("WARNING: 0 data received from archiver")
        
    #add secs and ns
    for i in xrange(0, len(secs)):
        secs[i] += float(nanos[i])/pow(10, 9) #1ns = 10^-9sec
    print "..end split data received(%f s)\n" %(time.time()-startProgram)

    print "from", dateTimeStart, "to", dateTimeStop
    print "Elapsed time:", elapsedTime, "sec"
    print "URL:", url

    if pvType == SCALAR:
        # check type
        if vals is list:
            exit(pvName + "is a list, not a scalar")

        print "Nb of data available:", len(vals)
        # display
        for i in range(len(vals)):
            print i+1, ":", vals[i], secondsToStrLocalTime(secs[i])

    elif pvType == WAVEFORM:
        # check type
        if vals is int or vals is float:
            exit(pvName + "is a scalar")
        elif len(vals[0]) >= SIZE_MIN_IMAGE2D:
            exit(pvName + "is 2D-image, not a waveform")

        print "Nb of waveform(s) available:", len(vals)
        print "Waveform size:", len(vals[0])
        # display
        for i in range(len(vals)):
            print i+1, ":", vals[i], secondsToStrLocalTime(secs[i])

    elif pvType == IMAGE_2D:
        # check type
        if vals is int or vals is float:
            exit(pvName + "is a scalar")
        elif len(vals[0]) < SIZE_MIN_IMAGE2D:
            exit(pvName + "is a waform, not a 2D-image")

        print "start from vector to matrix.. (%f s)" %(time.time()-startProgram)
        nbOfImage = len(vals) # vals is a list of images
        print "number of images:", nbOfImage

        nbOfPixels = len(vals[0])
        print "number of pixels per image:", nbOfPixels
        # check resolution
        if nbOfPixels == 2304000:
            x = 1920
            y = 1200
        elif nbOfPixels == 576000:
            x = 960
            y = 600
        else:
            exit("ERROR: resolution")
        print "resolution:", x, "x", y


        # reshape: vector to matrix (2D image)
        for i in range(nbOfImage):
            vmin2D=1000
            vmax2D=8000

            timeRef = secs[i] - secs[0] # time ref of first image is 0 sec
            print i+1, ': {:.3f}'.format(timeRef), "sec", secondsToStrLocalTime(secs[i])
            
            fig, (ax1, ax2) = plt.subplots(1,2)
            title = pvName + ": " + '{:.3f}'.format(timeRef) + " sec, " + secondsToStrLocalTime(secs[i])
            fig.suptitle(title, fontsize=12)
            
            # histo
            histY, histX, Null = ax2.hist(vals[i], normed=False, histtype='step', bins='auto')
            # marker
            ax2.plot([vmin2D, vmin2D], [min(histY), max(histY)], 'k-', color='red', label="min value 2D-image")
            ax2.plot([vmax2D, vmax2D], [min(histY), max(histY)], 'k-', color='orange', label="max value 2D-image")
            ax2.set_title('2D image histogram')
            
            

            # 1D array to 2D array (2D-image)
            data = np.array(vals[i]).reshape((y,x)) # vals[i] is a list (1D) which contains 1 image (2D)
            vals[i] = data

            # plot image
            ax1.set_title('2D image')
            image2D = ax1.imshow(data, vmin=vmin2D, vmax=vmax2D)
            # color bar
            divider4 = make_axes_locatable(ax1)
            cax1 = divider4.append_axes("right", size="5%", pad=0.05)
            plt.colorbar(image2D, cax=cax1)
            ax2.legend()

            # comment the here if youdon't want to plot data
            plt.show()

        print "..end from vector to matrix (%f s)" %(time.time()-startProgram)
    else:
        exit("PV is neither a SCALAR niether a WAVEFORM neither a IMAGE_2D")

    return secs, vals


variableToGet = [
                    #"timing:cmdTrep_epics", SCALAR,
                    "NPM:1CAMFITS-PeakMuActual", SCALAR,
                    "LHE:Sum_BPM1", WAVEFORM,
                    "NPM:1CAMOBSIMAGE-ArrayData", IMAGE_2D,
                ]

startDate = "2018-02-19 16:17:00" 
stopDate = "2018-02-19 16:17:04" 

nbVariableToGet = len(variableToGet)

startProgram = time.time()
for i in range(0, nbVariableToGet, 2): # from 0 to nbVariableToGet with a step of 2
    secs, vals = getDataFromArchiver(variableToGet[i], variableToGet[i+1], startDate, stopDate)

    #store in a file 
    SEPARATOR = ";"
    np.set_printoptions(threshold=np.nan) # full data printed
    fileName = variableToGet[i] + "_from_" + startDate + "_to_" + stopDate + ".txt"
    file = open(fileName, "w")
    file.write("sec" + SEPARATOR + str(secs) + "\n")
    print "start writing data in file.. (%f s)" %(time.time()-startProgram)
    file.write("val" + SEPARATOR + str(vals) + "\n")
    print "..end writing data in file (%f s)\n" %(time.time()-startProgram)
    file.close

    print "\n"

