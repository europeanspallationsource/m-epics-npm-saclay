import epics
import time
# print("I'm triggered")

# brake state:
#     - 0: break on
#     - 1: break off

# PV prefix
P = "IPHI:"
M = "MTR1:"

# remove break
pvName = P + M + 'BRAKE_S'
print(pvName)
PV_freinS = epics.PV(pvName) 
PV_freinS.put(True) # remove break
print("Remove the break")

# check break is removed
PV_freinR = epics.PV(P + M + 'BRAKE_R') 
while( PV_freinR.get() == 0): # wait until brake is off
    pass
print("[CHECKED] Remove the break")

# get position asked
PV_position = epics.PV(P + M + 'POSITION_S') 
positionAsked = PV_position.get()
print("Position asked is:")
print(positionAsked)

# move to position
PV_console = epics.PV(P + 'PMAC:' + 'ASK') 
commandPMAC = '#1J=' + str((positionAsked-5.5)*(-180114)/(95.5-5.5)) #conversion mm to cts
PV_console.put(commandPMAC)
print('commandPMAC: ')
print(commandPMAC)

# wait motor start the move
time.sleep(2) #0.5sec
print('end sleep (0.5 sec)')

# end start of move
PV_actualVelocity = epics.PV(P + M + 'ACTUAL_VEL_R') 
print('wait for velocity equals 0')
while( PV_actualVelocity.get() != 0): # wait until motor stops
    pass

# brake on
PV_freinS.put(False) # put break
print('break on')

# wait break on
time.sleep(0.5) #0.5sec
print('end sleep (1sec)')

# check break is on
if PV_freinR.get() == False:
    print("[CHECKED] break on")
else:
    print("[FAILED] break on")
