include $(EPICS_ENV_PATH)/module.Makefile

#STARTUPS = startup/emittanceTest.cmd
DOC = doc/README.md
OPIS += opi/testEmitance_1100x600.opi
OPIS += opi/enable_HV.js
OPIS += opi/cea.png
OPIS += opi/ess2.png
OPIS += opi/irfu.png
#USR_DEPENDENCIES = <module>,<version>

MISCS += misc/npm_motor.CFG
MISCS += misc/move_to_position.py
MISCS +=protocol/pmacVariables.proto
